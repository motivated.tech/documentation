# Cordova and EmberJS

## Folder Structure
For a new project start making a Cordova project:

`cordova create hello com.example.hello HelloWorld`

`cd hello`

Don't forget to add your platforms:

`cordova platform add ios --save`

`cordova platform add android --save`


Now we are ready to make the EmberJS app. Use ember-cli to generate the ember application inside the newly created Cordova project folder.

`ember new ember-app -sg`

*The name should be something different from cordova's folder names (i.e. not `www` or `hooks`)*


## EmberJS

There are a few things that you need to modify on your EmberJS app in order for it to be ready to run inside Cordova and use Cordova's plugins.

- In  `app/index.html` add `<script src="cordova.js"></script>` inside the `<body>` tag.

- In `config/environment.js` set the `locationType` to `hash`.

- In `.ember-cli` add `"output-path": "../www"`

- Generate a initializer using `ember g initializer cordova-device-ready`

- Paste this snippet inside `initializers/cordova-device-ready`:

```
import isOnCordova from '../utils/is-on-cordova';

export function initialize(application) {
  application.deferReadiness();
  document.addEventListener('deviceready', function() {
  		application.advanceReadiness();
  	}, false);
  if(!isOnCordova()){
  	document.dispatchEvent(new Event('deviceready'));
  }
}

export default {
  name: 'cordova-device-ready',
  initialize
};
```

- Generate a utility module using `ember g util is-on-cordova`

- Paste this snippet inside `utils/is-on-cordova.js`:

```
export default function isOnCordova() {
  return !!window.cordova;
}
```
